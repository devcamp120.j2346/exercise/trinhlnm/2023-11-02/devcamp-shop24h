const NotFound = () => {
    return (
        <div style={{margin: "10rem auto", textAlign: "center"}}>Not found</div>
    )
}

export default NotFound
