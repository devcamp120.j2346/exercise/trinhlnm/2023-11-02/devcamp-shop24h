import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "../App.css";
import ProductPicked from "../components/ProductPicked";

const GioHang = () => {
    const { dsSpCart } = useSelector(reduxData => reduxData.productReducer);
    const [productsPicked, setProductsPicked] = useState([]);
    const [subTotal, setSubTotal] = useState(0);

    useEffect(() => {
        let fetchProductsById = Promise.all(dsSpCart.map(async (e) => {
            try {
                let response = await (
                    await fetch("http://localhost:8080/api/v1/products/" + e)
                ).json();
                return response.data;
            }
            catch (error) {
                return;
            }
        }));

        fetchProductsById.then(data => {
            setProductsPicked(data);
            var vSubTotal = 0;
            for(let bI = 0; bI < productsPicked.length; bI++) {
                var vQuantity = localStorage.getItem(productsPicked[bI]._id);
                var vQuantityNumber = parseInt(vQuantity);
                var vPrice = productsPicked[bI].buyPrice * vQuantityNumber;
                vSubTotal = vSubTotal + vPrice;
            }
            setSubTotal(vSubTotal);
        });
    }, [productsPicked]);

    const handleSubTotalChange = (changeValue) => {
        setSubTotal(subTotal + changeValue);
    }

    console.log(productsPicked);

    return (
        <div style={{ padding: "3rem" }}>
            <div style={{marginTop: "2rem"}}>
                <div className="row" style={{fontWeight: "700"}}>
                    <div className="col-6">Products</div>
                    <div className="col-1 text-center">Price</div>
                    <div className="col-3 text-center">Quantity</div>
                    <div className="col-1 text-center">Total</div>
                </div>
                {productsPicked ? <div>
                    {
                        productsPicked.map(e => <ProductPicked e={e} changeSubTotal={handleSubTotalChange}/>)
                    }
                </div> : <></>}
            </div>

            <div style={{ display: "flex", justifyContent: "space-between", margin: "3rem auto" }}>
                <button className='shop-btn' style={{backgroundColor: "#e5e5e5", color: "#535353"}}>CONTINUE SHOPPING</button>
                <button className='shop-btn' style={{backgroundColor: "#e5e5e5", color: "#535353"}}>UPDATE CART</button>
            </div>

            <div className="row" style={{padding: "0rem"}}>
                <div className="col-md-7">
                    <div style={{fontWeight: "700", marginBottom: "1rem"}}>Discount Codes</div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                        <input className="coupon-input" placeholder="Enter your coupon code" />
                        <button className='shop-btn' style={{margin: "0px", backgroundColor: "#595959"}}>APPLY COUPON</button>
                    </div>
                </div>
                <div className="col-md-5" style={{fontWeight: "700", padding: "1rem", backgroundColor: "#f7f7f7"}}>
                    <div>Cart Total</div>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <div style={{fontSize: "0.9rem"}}>Subtotal</div>
                        <div style={{color: "#c91b1b"}}>${subTotal}</div>
                    </div>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <div style={{fontSize: "0.9rem"}}>Total</div>
                        <div style={{color: "#c91b1b"}}>$0</div>
                    </div>
                    <button className='shop-btn' style={{backgroundColor: "#007c00", width: "100%"}}>PROCEED TO CHECKOUT</button>
                </div>
            </div>
        </div>
    );
}

export default GioHang;