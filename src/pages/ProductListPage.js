import BreadCrumb from "../components/BreadCrumb";
import "bootstrap/dist/css/bootstrap.min.css";
import ProductFilter from "../components/products/ProductFilter";
import ProductShow from "../components/products/ProductShow";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { clearFilterAction } from "../actions/filter.action";

const urls = [
    { name: "Trang chủ", url: "/home" },
    { name: "Danh mục sản phẩm", url: "/products" },
]

const ProductListPage = () => {
    const [seed, setSeed] = useState(1);
    const reset = () => {
         setSeed(Math.random());
    }

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(clearFilterAction());
        reset();
    }, []);

    return (
        <div key={seed} style={{ padding: "3rem" }}>
            <BreadCrumb routes={urls} />

            <div className="row" style={{ paddingTop: "3rem" }}>
                <div className="col-md-3" style={{ paddingRight: "2rem" }}>
                    <ProductFilter/>
                </div>

                <div className="col-md-9">
                    <ProductShow/>
                </div>
            </div>
        </div>
    );
}

export default ProductListPage;