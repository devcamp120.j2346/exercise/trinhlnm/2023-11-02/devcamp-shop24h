import { useState } from "react";
import "../App.css"
import { useNavigate } from "react-router-dom";

async function postFetch(data, navigate) {
    
    try {
        const response = await fetch("http://localhost:8080/api/auth/signup", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });

        const result = await response.json();
        console.log(result);
        if(result.message == "Create user successfully") {
            navigate("/login");
        } else {
            alert(result.message);
        }
    } catch (error) {
        console.error(error);
        alert("Error");
    }
}

const SignUpPage = () => {
    const navigate = useNavigate();
    const [inputUsername, setInputUsername] = useState("");
    const [inputPass, setInputPass] = useState("");
    const [inputRePass, setInputRePass] = useState("");

    const onInputUsernameChange = (e) => {
        setInputUsername(e.target.value);
    }

    const onInputPassChange = (e) => {
        setInputPass(e.target.value);
    }

    const onInputRePassChange = (e) => {
        setInputRePass(e.target.value);
    }

    const onGetStartedClicked = () => {
        var vCheck = true;
        if (inputUsername == "") {
            alert("User name required!");
            vCheck = false;
        }

        if (inputPass == "") {
            alert("Password required!");
            vCheck = false;
        }

        if (inputPass != inputRePass) {
            alert("Your password not confirmed!");
            vCheck = false;
        }

        if (vCheck) {
            postFetch({
                username: inputUsername,
                password: inputPass
            }, navigate);
        }
    }

    const onLoginBtnClicked = () => {
        navigate("/login");
    }

    return (
        <div style={{ minHeight: "100vh", backgroundColor: "#f46060", marginTop: "3rem", display: "flex", justifyContent: "center", alignItems: "center" }}>
            <div style={{ backgroundColor: "white", width: "40%", padding: "3rem", margin: "2rem" }}>
                <img src={require("../assets/images/chef-hat.png")} style={{ width: "30%" }} />
                <div style={{ color: "#f46060", fontSize: "2rem", fontWeight: "600", marginTop: "2rem" }}>WELCOME</div>
                <div style={{ fontSize: "1.5rem", fontWeight: "500", color: "#575353", marginBottom: "1rem" }}>Sign in to continue</div>
                <input onChange={onInputUsernameChange} className="login-input" placeholder="User Name" />
                <input onChange={onInputPassChange} className="login-input" placeholder="Password" />
                <input onChange={onInputRePassChange} className="login-input" placeholder="Re-enter Password" />
                <div onClick={onGetStartedClicked} style={{ color: "black", fontWeight: "600", fontSize: "17px", lineHeight: "4rem", marginBottom: "2rem" }}>Get Started &rarr;</div>
                <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                    <div>Already have an account?</div>
                    <button onClick={onLoginBtnClicked} style={{ border: "0px", borderRadius: "10px", padding: "1rem 2rem", backgroundColor: "#f46060", color: "white", fontWeight: "500" }}>LOGIN</button>
                </div>
            </div>
        </div>
    );
}

export default SignUpPage;