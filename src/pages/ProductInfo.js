import { useSelector } from "react-redux";
import BreadCrumb from "../components/BreadCrumb";
import InfoComponent from "../components/productDetail/InfoComponent";
import DesComponent from "../components/productDetail/DesComponent";
import RelatedProduct from "../components/productDetail/RelatedProduct";


const ProductInfo = () => {

    const { pending, product} = useSelector(reduxData => reduxData.productReducer);

    console.log(product);

    const urlsInfo = [
        { name: "Trang chủ", url: "/home" },
        { name: "Danh mục sản phẩm", url: "/products" },
        { name: product.name },
    ]

    return (
        <div style={{ padding: "3rem" }}>
            {
                pending ? <></> : <div>
                    <BreadCrumb routes={urlsInfo} />
                    <div style={{margin: "3rem auto 5rem auto"}}>
                        <InfoComponent product={product} />
                    </div>
                    <DesComponent des={product.description}/>
                    <div style={{margin: "6rem auto auto"}}>
                        <RelatedProduct/>
                    </div>
                </div>
            }
        </div>
    );
}

export default ProductInfo;