import { CLEAR_FILTER, CLEAR_PAGE, INPUT_GIA_MAX_CHANGE, INPUT_GIA_MIN_CHANGE, INPUT_LOAISP_CHANGE, INPUT_TENSP_CHANGE, PAGE_CHANGE_PAGINATION, PRODUCT_FETCH_ERROR, PRODUCT_FETCH_PENDING, PRODUCT_FETCH_SUCCESS } from "../constants/filter.constant";

const initialState = {
    pending: false,
    products: [],
    tenSp: "",
    giaMax: 0,
    giaMin: 0,
    loaiSp: "",
    limit: 9,
    noPage: 0,
    currentPage: 1
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCT_FETCH_PENDING:
            state.pending = true;
            break;
        case PRODUCT_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalProduct / state.limit);
            state.products = action.data;
            break;
        case PRODUCT_FETCH_ERROR:
            break;
        case INPUT_TENSP_CHANGE:
            state.tenSp = action.payload;
            break;
        case INPUT_GIA_MAX_CHANGE:
            state.giaMax = action.payload;
            break;
        case INPUT_GIA_MIN_CHANGE:
            state.giaMin = action.payload;
            break;
        case INPUT_LOAISP_CHANGE:
            if(action.payload.check == true) {
                console.log(action.payload.id);
                state.loaiSp = state.loaiSp.concat("&productType=" + action.payload.id);
            } else {
                state.loaiSp = (state.loaiSp).replace("&productType=" + action.payload.id, "");
            }
            break;
        case PAGE_CHANGE_PAGINATION:
            state.currentPage = action.payload;
            break;
        case CLEAR_FILTER:
            state.tenSp = "";
            state.giaMax = 0;
            state.giaMin = 0;
            state.loaiSp = "";
            state.products = [];
            break;
        case CLEAR_PAGE:
            state.currentPage = 1;
            state.noPage = 0;
            break;
        default:
            break;
    }

    return { ...state };
}

export default userReducer;
