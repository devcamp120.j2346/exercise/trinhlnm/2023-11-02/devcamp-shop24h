import { BUTTON_ADD_TO_CART_CLICKED, PRODUCT_BY_ID_FETCH_ERROR, PRODUCT_BY_ID_FETCH_PENDING, PRODUCT_BY_ID_FETCH_SUCCESS, PRODUCT_RELATED_FETCH_ERROR, PRODUCT_RELATED_FETCH_PENDING, PRODUCT_RELATED_FETCH_SUCCESS } from "../constants/product.constant";

const initialState = {
    product: {},
    pending: false,
    dsSpCart: [],
    productRelated: []
}

//localStorage.clear();
var dsSpCartString = localStorage.getItem("dsSpCart");
if (dsSpCartString) {
    const dsSpCart = JSON.parse(dsSpCartString);
    initialState.dsSpCart = dsSpCart;
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCT_BY_ID_FETCH_PENDING:
            state.pending = true;
            break;
        case PRODUCT_BY_ID_FETCH_SUCCESS:
            state.pending = false;
            state.product = action.data.data;
            state.productRelated = action.relatedProducts.data;
            break;
        case PRODUCT_BY_ID_FETCH_ERROR:
            break;
        case BUTTON_ADD_TO_CART_CLICKED:
            if (!(state.dsSpCart.some((e) => e == action.payload))) {
                state.dsSpCart.push(action.payload);
                localStorage.setItem("dsSpCart", JSON.stringify(state.dsSpCart));
                localStorage.setItem(action.payload, 1);
            }
            break;
        default:
            break;
    }
    return { ...state };
}

export default productReducer;