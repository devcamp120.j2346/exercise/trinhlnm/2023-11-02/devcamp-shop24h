import GioHang from "./pages/GioHang";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import NotFound from "./pages/NotFound";
import ProductInfo from "./pages/ProductInfo";
import ProductListPage from "./pages/ProductListPage";
import SignUpPage from "./pages/SignUpPage";

const routes = [
    {path: "/", element: <SignUpPage/>},
    {path: "/login", element: <LoginPage/>},
    {path: "/home", element: <HomePage/>},
    {path: "/card", element: <GioHang/>},
    {path: "/products", element: <ProductListPage/>},
    {path: "/products/:productId", element: <ProductInfo/>},
    {path: "*", element: <NotFound/>}
];

export default routes;