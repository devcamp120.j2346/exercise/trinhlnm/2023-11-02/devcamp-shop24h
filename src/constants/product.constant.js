export const PRODUCT_BY_ID_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy product từ id";

export const  PRODUCT_BY_ID_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy product từ id";

export const  PRODUCT_BY_ID_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy product từ id";

export const BUTTON_ADD_TO_CART_CLICKED = "Button add to cart được ấn";