export const PRODUCT_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách product";
export const PRODUCT_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách product";
export const PRODUCT_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách product";

export const INPUT_TENSP_CHANGE = "Input tên sản phẩm change";
export const INPUT_GIA_MAX_CHANGE = "Input giá max change";
export const INPUT_GIA_MIN_CHANGE = "Input giá min change";
export const INPUT_LOAISP_CHANGE = "Input loại sản phẩm change";
export const PAGE_CHANGE_PAGINATION = "Sự kiện thay đổi trang";

export const CLEAR_FILTER = "Xóa trắng dữ liệu filter";
export const CLEAR_PAGE = "Đặt lại curent page";