import { useState } from "react";
import "../../App.css";

const DesComponent = ({ des }) => {
    const [expand, setExpand] = useState(false);

    const handleViewAllBtnClicked = () => {
        setExpand(!expand);
    }

    return (
        <div style={{ textAlign: "center", position: "relative", paddingBottom: "5rem" }}>
            <div style={{height: expand ? "auto" : "20rem", overflow: "hidden"}}>
                <div style={{ fontWeight: "700" }}>Description</div>
                <div>
                    {des}
                </div>
                <img src="https://upload.wikimedia.org/wikipedia/commons/f/f5/Best_Buy_Logo.svg" />
            </div>
            <div style={{ position: "absolute", bottom: "1rem", textAlign: "center", width: "100%", backgroundColor: "white" }}>
                <button onClick={handleViewAllBtnClicked} className='shop-btn'>{expand ? "VIEW LESS" : "VIEW ALL"}</button>
            </div>
        </div>
    );
}

export default DesComponent;