import { useSelector } from "react-redux";
import ProductCart from "../ProductCart";
import "bootstrap/dist/css/bootstrap.min.css";

const RelatedProduct = () => {
    const { productRelated, product } = useSelector(reduxData => reduxData.productReducer);

    console.log(productRelated);

    return (
        <div>
            <div style={{ fontWeight: "700", marginBottom: "1rem" }}>Related Products</div>
            <div className="row" style={{padding: "0rem 2rem"}}>
                {
                    productRelated.map((e) => {
                        return (
                            <ProductCart e={e}/>
                        )
                    })
                }
            </div>
        </div>
    );
}

export default RelatedProduct;