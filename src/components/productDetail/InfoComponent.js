import "bootstrap/dist/css/bootstrap.min.css";
import "../../App.css"
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useDispatch } from "react-redux";
import { btnAddToCartClickAction } from "../../actions/product.action";

const InfoComponent = ({product}) => {
    const dispatch = useDispatch();

    const btnAddToCartClicked = (id) => {
        dispatch(btnAddToCartClickAction(id));
    }

    return (
        <div className="row">
            <div className="col-md-4 text-center">
                <img src={product.imageUrl} style={{width: "80%"}}/>
            </div>
            <div className="col-md-8">
                <div style={{fontSize: "2rem", fontWeight: "700"}}>{product.name}</div>
                <div style={{margin: "1rem auto"}}>{product.type.name}</div>
                <div>{product.description}</div>
                <div style={{color: "#c91b1b", fontSize: "1.3rem", fontWeight: "700", margin: "1rem auto"}}>$ {product.buyPrice}</div>
                <div style={{display: "flex", alignItems: "center"}}>
                    <RemoveCircleIcon sx={{color: "grey"}}/>
                    <div style={{fontSize: "1rem", fontWeight: "500", margin: "auto 5px"}}>1</div>
                    <AddCircleIcon sx={{color: "grey"}}/>
                </div>
                <button onClick={() => btnAddToCartClicked(product._id)} className="shop-btn">ADD TO CART</button>
            </div>
        </div>
    );
}

export default InfoComponent;