import "bootstrap/dist/css/bootstrap.min.css";
import "../App.css"
import { NavLink } from "react-router-dom";

const BreadCrumb = ({ routes }) => {
    return (
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                {
                    routes.map((e, i) => {
                        return (
                            <NavLink to={e.url}>
                                <div style={{display: "flex", color: "black"}}>
                                    <span style={{fontWeight: "500"}}>{e.name}</span>
                                    {
                                        i == (routes.length - 1) ? <></> : <div style={{fontWeight: "700", margin: "auto 8px"}}>&raquo;</div>
                                    }
                                </div>
                            </NavLink>
                        );
                    })
                }
            </ol>
        </nav>
    );
}

export default BreadCrumb;
