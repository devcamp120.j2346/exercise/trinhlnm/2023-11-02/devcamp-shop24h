import { useNavigate } from "react-router-dom";

const LogoComponent = () => {
    const navigate = useNavigate();

    const handleLogoClick = () => {
        navigate("/home");
    }

    return (
        <div onClick={handleLogoClick}>
            <span className="navbar-brand mb-0 h1" style={{fontSize: "1.5rem", fontWeight: "700"}}>DevCamp</span>
        </div>
    );
}

export default LogoComponent;