import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faUserCircle } from '@fortawesome/free-regular-svg-icons';
import "../../App.css";
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const IconNavBarComponent = () => {
    const navigate = useNavigate();

    const { dsSpCart } = useSelector((reduxData) => {
        return reduxData.productReducer;
    });

    const iconCartClicked = () => {
        navigate("/card");
    }

    return (
        <div>
            <FontAwesomeIcon icon={faBell} />
            <FontAwesomeIcon icon={faUserCircle} className='mx-3' />
            <span onClick={iconCartClicked}>
                <img src={require("../../assets/images/shopping-cart.png")} style={{ height: "0.9em", marginTop: "-2px" }} />
                <span class="so-sp-mess">{dsSpCart.length}</span>
            </span>
        </div>
    );
}

export default IconNavBarComponent;