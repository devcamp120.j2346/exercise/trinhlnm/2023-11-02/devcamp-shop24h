import 'bootstrap/dist/css/bootstrap.min.css';

import LogoComponent from './Logo';
import IconNavBarComponent from './IconNavBar';


const HeaderComponent = () => {
    return (
        <nav className="navbar fixed-top container">
            <LogoComponent />
            <IconNavBarComponent />
        </nav>
    );
}

export default HeaderComponent;