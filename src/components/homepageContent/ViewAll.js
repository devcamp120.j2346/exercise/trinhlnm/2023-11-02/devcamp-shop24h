import { useNavigate } from "react-router-dom";
import "../../App.css";

const ViewAll = () => {
    const navigate = useNavigate();

    const onBtnViewAllClick = () => {
        navigate("/products");
    }

    return (
        <div style={{textAlign: "center", marginBottom: "6rem", marginTop: "1rem" }}>
            <button className='shop-btn' onClick={onBtnViewAllClick}>VIEW ALL</button>
        </div>
    );
}

export default ViewAll;