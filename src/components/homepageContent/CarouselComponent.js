import React, { useState } from 'react';
import {
    Carousel,
    CarouselItem,
    Button,
} from 'reactstrap';
import "../../App.css";
import 'bootstrap/dist/css/bootstrap.min.css';

const items = [
    {
        src: 'https://picsum.photos/id/123/1200/400',
        altText: 'Slide 1',
        caption: 'Slide 1',
        key: 1,
    },
    {
        src: 'https://picsum.photos/id/456/1200/400',
        altText: 'Slide 2',
        caption: 'Slide 2',
        key: 2,
    },
    {
        src: 'https://picsum.photos/id/678/1200/400',
        altText: 'Slide 3',
        caption: 'Slide 3',
        key: 3,
    },
];

const CarouselComponent = (args) => {
    console.log(args);
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    };

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    };

    const slides = args.data.map((item, index) => {
        return (
            <CarouselItem
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={index}
            >
                <div style={{ margin: "auto 8rem" }}>
                    <div className='row'>
                        <div className='col-md-6' style={{ padding: "2rem", display: "flex", alignItems: "center" }}>
                            <div>
                                <div style={{ fontWeight: "bold" }}>{item.type.name}</div>
                                <h1 style={{ marginBottom: "1.5rem" }}>{item.name}</h1>
                                <div>{item.description}</div>
                                <button className='shop-btn'>SHOP NOW</button>
                            </div>
                        </div>
                        <div className='col-md-6 text-center'>
                            <img src={item.imageUrl} style={{ width: "90%" }} />
                        </div>
                    </div>
                </div>
            </CarouselItem>
        );
    });

    return (
        <Carousel
            interval={null}
            style={{ margin: "3rem auto", position: "relative" }}
            activeIndex={activeIndex}
            next={next}
            previous={previous}
            {...args}
        >
            {slides}
            <Button onClick={previous} className='prev-btn' style={{ left: "2rem" }}>&#60;</Button>
            <Button onClick={next} className='prev-btn' style={{ right: "2rem" }}>&#62;</Button>
        </Carousel>
    );
}

export default CarouselComponent;