import { useState, useEffect } from 'react';
import CarouselComponent from './CarouselComponent';
import LastestProducts from './LastestProducts';
import ViewAll from './ViewAll';

const HomepageContent = () => {
    const [data, setData] = useState();
    const [data9, setData9] = useState();

    useEffect(() => {
        const dataFetch = async () => {
            const startTime = sessionStorage.getItem("startTime");
            var endTime = new Date().getTime() - parseInt(startTime);
            if (endTime > 240000) {
                //accessToken đã hết hạn
                const refreshToken = sessionStorage.getItem("refreshToken");
                try {
                    const refreshTokenRes = await fetch("http://localhost:8080/api/auth/refreshToken", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({ refreshToken: refreshToken }),
                    });
                    const refreshTokenData = await refreshTokenRes.json();

                    //lấy new access token và gọi lại api
                    const headers = { 'x-access-token': refreshTokenData.accessToken };
                    const resAgain = await (
                        await fetch("http://localhost:8080/api/v1/products?limit=3", { headers })
                    ).json();
                    const res9Again = await (
                        await fetch("http://localhost:8080/api/v1/products?limit=9", { headers })
                    ).json();

                    setData(resAgain.data);
                    setData9(res9Again.data);
                } catch (error) {
                    console.log(error);
                }
            } else {
                try {
                    const accessToken = sessionStorage.getItem("accessToken");
                    const headers = { 'x-access-token': accessToken };
                    const res = await (
                        await fetch("http://localhost:8080/api/v1/products?limit=3", { headers })
                    ).json();
                    const res9 = await (
                        await fetch("http://localhost:8080/api/v1/products?limit=9", { headers })
                    ).json();

                    setData(res.data);
                    setData9(res9.data);
                } catch (error) {
                    console.log(error);
                }
            }
        }

        dataFetch();
    }, []);

    return (
        <div style={{ textAlign: "center" }}>
            {data ? <CarouselComponent data={data} /> : <div style={{ margin: "10rem auto 2rem auto" }}>Carousel</div>}
            {data9 ? <LastestProducts data={data9} /> : <div style={{ marginBottom: "2rem" }}>Lastest Products</div>}
            <ViewAll />
        </div>
    );
}

export default HomepageContent;