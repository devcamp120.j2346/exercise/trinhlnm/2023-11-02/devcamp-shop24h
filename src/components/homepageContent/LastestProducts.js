import 'bootstrap/dist/css/bootstrap.min.css';
import ProductCart from '../ProductCart';

const LastestProducts = ({ data }) => {
    return (
        <div className='container' style={{ marginTop: "6rem" }}>
            <h4 className='text-center' style={{ fontWeight: "700" }}>LATEST PRODUCT</h4>
            <div className='row' style={{ marginTop: "3rem" }}>
                {data.map((e) => {
                    return (
                        <ProductCart e={e} />
                    );
                })}
            </div>
        </div>
    );
}

export default LastestProducts;