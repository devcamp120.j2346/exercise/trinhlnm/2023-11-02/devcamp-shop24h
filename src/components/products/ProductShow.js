import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearPageAction, fetchProduct } from "../../actions/filter.action";
import ProductCart from "../ProductCart";
import ProductPagination from "./ProductPagination";
import { CircularProgress } from "@mui/material";

const ProductShow = () => {
    const dispatch = useDispatch();

    const { pending, products, currentPage, limit, tenSp, giaMax, giaMin, loaiSp } = useSelector(reduxData => reduxData.userReducer);

    useEffect(() => {
        dispatch(fetchProduct(currentPage, limit, tenSp, giaMax, giaMin, loaiSp));
    }, [currentPage, tenSp, giaMin, giaMax, loaiSp]);

    useEffect(() => {
        dispatch(clearPageAction());
    }, [tenSp, giaMin, giaMax, loaiSp]);

    return (
        <>
            {
                products ? <div>
                    {
                        pending ? <div style={{ textAlign: "center" }}><CircularProgress color="inherit" style={{ width: "2rem", height: "2rem" }} /></div>
                            : <div className="row">
                                {
                                    products.map((e) => {
                                        return <ProductCart e={e} />;
                                    })
                                }
                            </div>
                    }
                    <div style={{ textAlign: "end" }}>
                        <ProductPagination/>
                    </div>
                </div>
                    : <div style={{ margin: "8rem auto", textAlign: "center" }}>Danh mục sản phẩm</div>
            }
        </>
    );
}

export default ProductShow;