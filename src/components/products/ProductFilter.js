import { useDispatch, useSelector } from "react-redux";
import { clearPageAction, inputGiaMaxChangeAction, inputGiaMinChangeAction, inputLoaiSpChangeAction, inputTenSpChangeAction, valueLoaiSpChangeAction } from "../../actions/filter.action";
import "../../App.css";

const loaiSpList = [
    { id: "65333285fe7faadf0dbb7755", name: "Điện tử" },
    { id: "65333285fe7faadf0dbb7754", name: "Trang trí" },
    { id: "65333285fe7faadf0dbb7758", name: "Máy tính" },
    { id: "65333285fe7faadf0dbb775b", name: "Thiết bị" }
];

const ProductFilter = () => {
    const dispatch = useDispatch();

    const { tenSp, giaMax, giaMin } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    const inputTenSpChangeHandler = (event) => {
        dispatch(inputTenSpChangeAction(event.target.value));
    }

    const inputGiaMaxChangeHandler = (event) => {
        dispatch(inputGiaMaxChangeAction(event.target.value));
    }

    const inputGiaMinChangeHandler = (event) => {
        dispatch(inputGiaMinChangeAction(event.target.value));
    }

    const inputLoaiSpHandler = (check, id) => {
        dispatch(inputLoaiSpChangeAction({check: check, id: id}));
    }

    return (
        <div>
            <div style={{ fontWeight: "500" }}>Tên sản phẩm</div>
            <input onChange={inputTenSpChangeHandler} value={tenSp} style={{ width: "100%", marginTop: "1rem", marginBottom: "2rem" }} />

            <div style={{ fontWeight: "500" }}>Giá sản phẩm</div>
            <div style={{ display: "flex", justifyContent: "space-between", marginTop: "1rem", marginBottom: "2rem" }}>
                <input onChange={inputGiaMinChangeHandler} value={giaMin} type="number" style={{ width: "45%" }} />
                <div> - </div>
                <input onChange={inputGiaMaxChangeHandler} value={giaMax} type="number" style={{ width: "45%" }} />
            </div>

            <div style={{ fontWeight: "500", marginBottom: "1rem" }}>Loại sản phẩm</div>
            {
                loaiSpList.map((e) => {
                    return (
                        <div style={{ display: "flex", alignItems: "center", marginBottom: "1rem" }}>
                            <input onChange={(event) => inputLoaiSpHandler(event.target.checked, e.id)} type="checkbox" style={{ width: "1.3rem", height: "1.3rem", marginRight: "0.7rem" }} />
                            <div>{e.name}</div>
                        </div>
                    )
                })
            }
        </div>
    );
}

export default ProductFilter;