import { Pagination, PaginationItem } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { pageChangePagination } from "../../actions/filter.action";
import { useEffect, useState } from "react";

const ProductPagination = () => {
    const [seed, setSeed] = useState(1);
    const reset = () => {
         setSeed(Math.random());
    }

    const dispatch = useDispatch();

    const { noPage, tenSp, giaMin, giaMax, loaiSp } = useSelector(reduxData => reduxData.userReducer);

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    useEffect(() => {
        reset();
    }, [tenSp, giaMin, giaMax, loaiSp]);

    return (
        <Pagination
            key={seed}
            count={noPage}
            onChange={onChangePagination}
            sx={{ display: "inline-block" }} shape="rounded"
            renderItem={(item) => (
                <PaginationItem
                    {...item}
                    sx={{
                        "&.Mui-selected": {
                            backgroundColor: "black",
                            color: "white",
                            borderRadius: "0px"
                        }
                    }}
                />
            )}
        />
    );
}

export default ProductPagination;