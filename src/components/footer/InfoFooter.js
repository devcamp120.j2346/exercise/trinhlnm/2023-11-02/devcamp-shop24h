import 'bootstrap/dist/css/bootstrap.min.css';
import '../../App.css';

const InfoFooter = ({ tittle }) => {
    return (
        <div className='col'>
            <div className='footer-tittle'>{tittle}</div>
            <div>Help Center</div>
            <div>Contact Us</div>
            <div>Product Help</div>
            <div>Warranty</div>
            <div>Order Status</div>
        </div>
    );
}

export default InfoFooter;