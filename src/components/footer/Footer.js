import 'bootstrap/dist/css/bootstrap.min.css';
import SocialFooter from './SocialFooter ';
import InfoFooter from './InfoFooter';

const FooterComponent = () => {
    return (
        <footer className='row' style={{ fontSize: "0.9rem", backgroundColor: "#e9ecef", padding: "3rem 4rem 4rem 4rem" }}>
            <InfoFooter tittle={"PRODUCTS"} />
            <InfoFooter tittle={"SERVICES"} />
            <InfoFooter tittle={"SUPPORT"} />
            <SocialFooter />
        </footer>
    );
}

export default FooterComponent;