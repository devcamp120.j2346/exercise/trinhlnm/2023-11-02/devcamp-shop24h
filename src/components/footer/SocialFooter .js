import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faSquareInstagram, faYoutube, faTwitter } from '@fortawesome/free-brands-svg-icons';

const SocialFooter = () => {
    return (
        <div className='col'>
            <div style={{ fontSize: "1.8rem", fontWeight: "600", marginBottom: "0.2rem" }}>DevCamp</div>
            <div>
                <FontAwesomeIcon icon={faFacebook} style={{ margin: "0.5rem", height: "1rem" }} />
                <FontAwesomeIcon icon={faSquareInstagram} style={{ margin: "0.5rem", height: "1rem" }} />
                <FontAwesomeIcon icon={faYoutube} style={{ margin: "0.5rem", height: "1rem" }} />
                <FontAwesomeIcon icon={faTwitter} style={{ margin: "0.5rem", height: "1rem" }} />
            </div>
        </div>
    );
}

export default SocialFooter;