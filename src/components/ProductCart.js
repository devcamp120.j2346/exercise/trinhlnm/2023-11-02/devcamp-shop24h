import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { fetchProductById } from "../actions/product.action";

const ProductCart = ({e}) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const onCartClicked = () => {
        dispatch(fetchProductById(e._id));
        navigate("/products/" + e._id);
    }

    return (
        <div onClick={onCartClicked} className='col-md-4' style={{ marginBottom: "3rem" }}>
            <img src={e.imageUrl} style={{ width: "100%" }} />
            <div style={{ textAlign: "center", fontSize: "20px", fontWeight: "500", marginTop: "15px" }}>{e.name}</div>
            <div style={{ textAlign: "center" }}>
                <span style={{ fontWeight: "500", marginRight: "10px" }}>${e.buyPrice}</span>
                <span style={{ fontSize: "18px", fontWeight: "500", color: "red" }}>${e.promotionPrice}</span>
            </div>
        </div>
    );
}

export default ProductCart;