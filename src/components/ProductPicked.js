import { Divider, IconButton } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import { useEffect, useState } from "react";

const ProductPicked = ({e, changeSubTotal}) => {
    var vQuantity = localStorage.getItem(e._id);
    var vQuantityNumber = parseInt(vQuantity);
    const [quantity, setQuantity] = useState(vQuantityNumber);

    useEffect(() => {
        localStorage.setItem(e._id, quantity);
    }, [quantity]);

    const handleMinus = () => {
        if(quantity > 1) {
            setQuantity(quantity - 1);
            changeSubTotal(-e.buyPrice);
        }
    }

    const handlePlus = () => {
        setQuantity(quantity + 1);
        changeSubTotal(e.buyPrice);
    }

    return (
        <div className="row" style={{fontWeight: "600"}}>
            <Divider sx={{ my: "1rem", borderColor: "#adaaaa" }} />
            <div className="col-6 centerXY" style={{ justifyContent: "start" }}>
                <img src={e.imageUrl} style={{ maxHeight: "15rem", maxWidth: "15rem", marginRight: "2rem" }} />
                <div style={{fontSize: "1.3rem", fontWeight: "normal"}}>{e.name}</div>
            </div>
            <div className="col-1 centerXY">${e.buyPrice}</div>
            <div className="col-3 centerXY">
                <div className="counter">
                    <div style={{fontSize: "larger"}} onClick={handleMinus}>-</div>
                    <div class="counter-number">{quantity}</div>
                    <div onClick={handlePlus}>+</div>
                </div>
            </div>
            <div className="col-1 centerXY">${quantity * e.buyPrice}</div>
            <div className="col-1 centerXY" style={{ justifyContent: "end" }}>
                <IconButton aria-label="delete">
                    <CloseIcon />
                </IconButton>
            </div>
        </div>
    );
}

export default ProductPicked;