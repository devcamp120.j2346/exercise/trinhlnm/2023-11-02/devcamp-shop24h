import { CHANGE_PAG_SEED_NUMBER, CLEAR_FILTER, CLEAR_PAGE, INPUT_GIA_MAX_CHANGE, INPUT_GIA_MIN_CHANGE, INPUT_LOAISP_CHANGE, INPUT_TENSP_CHANGE, PAGE_CHANGE_PAGINATION, PRODUCT_FETCH_ERROR, PRODUCT_FETCH_PENDING, PRODUCT_FETCH_SUCCESS } from "../constants/filter.constant"

export const fetchProduct = (page, limit, tenSp, giaMax, giaMin, loaiSp) => {
    return async (dispatch) => {
        try {
            await dispatch({
                type: PRODUCT_FETCH_PENDING
            });

            var resProduct;
            //phân trang
            var params = new URLSearchParams({
                limit: limit,
                page: page
            });

            //lọc
            if(tenSp) {
                params.append("name", tenSp);
                //params.set("page", 1);
            }

            if(giaMax && giaMin) {
                params.append("minPrice", giaMin);
                params.append("maxPrice", giaMax);
            } else if(giaMin) { //buyPrice chữ màu đen
                params.append("minPrice", giaMin);
            } else if(giaMax) {
                params.append("maxPrice", giaMax);
            }

            const url = "http://localhost:8080/api/v1/products?" + params.toString() + loaiSp;
            const startTime = sessionStorage.getItem("startTime");
            var endTime = new Date().getTime() - parseInt(startTime);
            if (endTime > 240000) {
                //accessToken đã hết hạn
                const refreshToken = sessionStorage.getItem("refreshToken");

                const refreshTokenRes = await fetch("http://localhost:8080/api/auth/refreshToken", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({ refreshToken: refreshToken }),
                });
                const refreshTokenData = await refreshTokenRes.json();

                //lấy new access token và gọi lại api
                const headers = { 'x-access-token': refreshTokenData.accessToken };
                
                resProduct = await (
                    await fetch(url, { headers })
                ).json();
            } else {
                const accessToken = sessionStorage.getItem("accessToken");
                const headers = { 'x-access-token': accessToken };
                resProduct = await (
                    await fetch(url, { headers })
                ).json();
            }
            console.log(url);
            console.log(resProduct);

            return dispatch({
                type: PRODUCT_FETCH_SUCCESS,
                totalProduct: resProduct.total,
                data: resProduct.data
            });
        } catch (error) {
            return dispatch({
                type: PRODUCT_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const inputTenSpChangeAction = (inputValue) => {
    return {
        type: INPUT_TENSP_CHANGE,
        payload: inputValue
    }
}

export const inputGiaMaxChangeAction = (inputValue) => {
    return {
        type: INPUT_GIA_MAX_CHANGE,
        payload: inputValue
    }
}

export const inputGiaMinChangeAction = (inputValue) => {
    return {
        type: INPUT_GIA_MIN_CHANGE,
        payload: inputValue
    }
}

export const inputLoaiSpChangeAction = (inputValue) => {
    return {
        type: INPUT_LOAISP_CHANGE,
        payload: inputValue
    }
}

export const pageChangePagination = (page) => {
    return {
        type: PAGE_CHANGE_PAGINATION,
        payload: page
    };
}

export const clearFilterAction = () => {
    return {
        type: CLEAR_FILTER
    }
}

export const clearPageAction = () => {
    return {
        type: CLEAR_PAGE
    }
}