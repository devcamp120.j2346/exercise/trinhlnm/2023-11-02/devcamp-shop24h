import { BUTTON_ADD_TO_CART_CLICKED, PRODUCT_BY_ID_FETCH_ERROR, PRODUCT_BY_ID_FETCH_PENDING, PRODUCT_BY_ID_FETCH_SUCCESS, PRODUCT_RELATED_FETCH_ERROR, PRODUCT_RELATED_FETCH_PENDING, PRODUCT_RELATED_FETCH_SUCCESS } from "../constants/product.constant";

export const fetchProductById = (id) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: PRODUCT_BY_ID_FETCH_PENDING
            });

            const resProductById = await fetch("http://localhost:8080/api/v1/products" + "/" + id, requestOptions);

            const dataProductById = await resProductById.json();

            const resProductRelated = await fetch("http://localhost:8080/api/v1/products" + "?typeId=" + dataProductById.data.type._id + "&limit=6", requestOptions);

            const dataProductRelated = await resProductRelated.json();

            return dispatch({
                type: PRODUCT_BY_ID_FETCH_SUCCESS,
                data: dataProductById,
                relatedProducts: dataProductRelated
            });
        } catch (error) {
            return dispatch({
                type: PRODUCT_BY_ID_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const btnAddToCartClickAction = (id) => {
    return {
        type: BUTTON_ADD_TO_CART_CLICKED,
        payload: id
    }
}