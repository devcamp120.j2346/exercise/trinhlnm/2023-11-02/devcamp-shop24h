import BodyComponent from "./components/BodyComponent";
import FooterComponent from "./components/footer/Footer";
import HeaderComponent from "./components/header/Header";

function App() {
  return (
    <>
      <HeaderComponent/>
      <BodyComponent/>
      <FooterComponent/>
    </>
  );
}

export default App;
